using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SVS
{
    public static class PlacementHelper
    {
        //from video https://www.youtube.com/watch?v=sKNZ5CeQr1Y&list=PLcRSafycjWFcbaI8Dzab9sTy5cAQzLHoy&index=9

        public static List<Direction> FindNeighbor(Vector3Int position, ICollection<Vector3Int> collection)
        {
            List<Direction> neighborDirections = new List<Direction>();

            if(collection.Contains(position + Vector3Int.right))
            {
                neighborDirections.Add(Direction.Right);
            }
            if (collection.Contains(position + Vector3Int.left))
            {
                neighborDirections.Add(Direction.Left);
            }
            if (collection.Contains(position + new Vector3Int(0, 0, 1)))
            {
                neighborDirections.Add(Direction.Up);
            }
            if (collection.Contains(position - new Vector3Int(0, 0, 1)))
            {
                neighborDirections.Add(Direction.Down);
            }
            return neighborDirections;
        }

        internal static Vector3Int GetOffsetFromDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return new Vector3Int(0, 0, 1);
                case Direction.Down:
                    return new Vector3Int(0, 0, -1);
                case Direction.Left:
                    return Vector3Int.left;
                case Direction.Right:
                    return Vector3Int.right;
                default:
                    break;
            }
            throw new System.Exception("No Direction Such as" + direction);
        }

        //return the opposite direction returned
        public static Direction GetReverseDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return Direction.Down;
                case Direction.Down:
                    return Direction.Up;
                case Direction.Left:
                    return Direction.Right;
                case Direction.Right:
                    return Direction.Left;
                default:
                    break;
            }
            throw new System.Exception("No Direction Such as" + direction);
        }
    }
}
