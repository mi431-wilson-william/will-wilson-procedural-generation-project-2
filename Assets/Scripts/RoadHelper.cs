using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SVS
{

    //made from video https://www.youtube.com/watch?v=-sr_RFdMaz4&list=PLcRSafycjWFcbaI8Dzab9sTy5cAQzLHoy&index=8
    public class RoadHelper : MonoBehaviour
    {
        public GameObject roadStraight, roadCorner, road3way, road4way, roadEnd;
        Dictionary<Vector3Int, GameObject> roadDictionary = new Dictionary<Vector3Int, GameObject>();
        //placable roads
        Dictionary<Vector3Int, GameObject> validRoadDictionary = new Dictionary<Vector3Int, GameObject>();
        //downtown roads
        Dictionary<Vector3Int, GameObject> downtownRoadDictionary = new Dictionary<Vector3Int, GameObject>();
        HashSet<Vector3Int> fixRoadCanidates = new HashSet<Vector3Int>();

        public List<Vector3Int> GetRoadPositions()
        {
            return roadDictionary.Keys.ToList();
        }

        public List<Vector3Int> GetValidRoadPositions()
        {
            return validRoadDictionary.Keys.ToList();
        }

        public List<Vector3Int> GetDowntownRoadPositions()
        {
            return downtownRoadDictionary.Keys.ToList();
        }

        public void PlaceStreetPositions(Vector3 startPosition, Vector3Int direction, int length, bool exception = false)
        {
            var rotation = Quaternion.identity;
            if(direction.x == 0)
            {
                rotation = Quaternion.Euler(0, 90, 0);
            }
            for (int i = 0; i < length; i++)
            {
                var position = Vector3Int.RoundToInt(startPosition + direction * i);
                if (roadDictionary.ContainsKey(position))
                {
                    continue;
                }
                var road = Instantiate(roadStraight, position, rotation, transform);
                roadDictionary.Add(position, road);

                //if the road is an exception, don't add it to the list of placable road positions
                if (!exception)
                {
                    validRoadDictionary.Add(position, road);
                } else
                {
                    downtownRoadDictionary.Add(position, road);
                }

                if(i == 0 || i == length - 1)
                {
                    fixRoadCanidates.Add(position);
                }
            }
        }

        //from video https://www.youtube.com/watch?v=sKNZ5CeQr1Y&list=PLcRSafycjWFcbaI8Dzab9sTy5cAQzLHoy&index=9
        public void FixRoad()
        {
            foreach (var position in fixRoadCanidates)
            {
                List<Direction> neighborDirections = PlacementHelper.FindNeighbor(position, roadDictionary.Keys);

                Quaternion rotation = Quaternion.identity;

                //place the correct road type based on its neighbors context

                //spawn the road end
                if (neighborDirections.Count == 1)
                {
                    
                    Destroy(roadDictionary[position]);
                    if (neighborDirections.Contains(Direction.Down))
                    {
                        rotation = Quaternion.Euler(0, 90, 0);
                    }
                    else if (neighborDirections.Contains(Direction.Left))
                    {
                        rotation = Quaternion.Euler(0, 180, 0);
                    }
                    else if (neighborDirections.Contains(Direction.Up))
                    {
                        rotation = Quaternion.Euler(0, -90, 0);
                    }

                    roadDictionary[position] = Instantiate(roadEnd, position, rotation, transform);
                }
                //spawn a straightroad or corner
                else if (neighborDirections.Count == 2)
                {
                    //if it is a straight road
                    if(neighborDirections.Contains(Direction.Up) && neighborDirections.Contains(Direction.Down)
                        || neighborDirections.Contains(Direction.Right) && neighborDirections.Contains(Direction.Left))
                    {
                        continue;
                    }
                    Destroy(roadDictionary[position]);

                    //if its a corner
                    if (neighborDirections.Contains(Direction.Up) && neighborDirections.Contains(Direction.Right))
                    {
                        rotation = Quaternion.Euler(0, 90, 0);
                    }
                    else if (neighborDirections.Contains(Direction.Right) && neighborDirections.Contains(Direction.Down))
                    {
                        rotation = Quaternion.Euler(0, 180, 0);
                    }
                    else if (neighborDirections.Contains(Direction.Down) && neighborDirections.Contains(Direction.Left))
                    {
                        rotation = Quaternion.Euler(0, -90, 0);
                    }

                    roadDictionary[position] = Instantiate(roadCorner, position, rotation, transform);

                }
                //spawn a 3-way street
                else if (neighborDirections.Count == 3)
                {

                    Destroy(roadDictionary[position]);
                    //rotate the 3-way street 
                    if (neighborDirections.Contains(Direction.Right) 
                        && neighborDirections.Contains(Direction.Down)
                        && neighborDirections.Contains(Direction.Left))
                    {
                        rotation = Quaternion.Euler(0, 90, 0);
                    }
                    else if (neighborDirections.Contains(Direction.Down) 
                        && neighborDirections.Contains(Direction.Left)
                        && neighborDirections.Contains(Direction.Up))
                    {
                        rotation = Quaternion.Euler(0, 180, 0);
                    }
                    else if (neighborDirections.Contains(Direction.Left) 
                        && neighborDirections.Contains(Direction.Up)
                        && neighborDirections.Contains(Direction.Right))
                    {
                        rotation = Quaternion.Euler(0, -90, 0);
                    }

                    roadDictionary[position] = Instantiate(road3way, position, rotation, transform);
                }
                //spawn a 4-way road
                else
                {
                    Destroy(roadDictionary[position]);
                    roadDictionary[position] = Instantiate(road4way, position, rotation, transform);
                }
            }
        }
    }
}
