using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace SVS
{

    //variables for generation
    [System.Serializable]
    public class GenerationVariables
    {
        public Rule[] rules;
        [Range(0, 20)]
        public int iterationLimit = 1;

        //this was from vid ** https://www.youtube.com/watch?v=srLzIQNOBBE&list=PLcRSafycjWFcbaI8Dzab9sTy5cAQzLHoy&index=7
        public bool randomIgnoreRuleModifier = true;
        [Range(0, 1)]
        public float chanceToIgnoreRule = 0.3f;
    }

    public class LSystemGenerator : MonoBehaviour
    {
        public string rootSentence;

        public int highwayTurnLimit = 2;

        [SerializeField]
        public GenerationVariables highwayVariables;

        [SerializeField]
        public GenerationVariables sideStreetVariables;

        //this was from vid ** https://www.youtube.com/watch?v=srLzIQNOBBE&list=PLcRSafycjWFcbaI8Dzab9sTy5cAQzLHoy&index=7

        private void Start()
        {
            //Debug.Log(GenerateSentence());
        }


        //how I'm imagining it could work;
        //generate the highway
        //get the sentence
        //then pass that sentence and generate the streets

        //function called by Visualizer to generate the sentence
        //param word = root sentence specified in the object
        public string GenerateSentence(string word = null)
        {
            if (word == null)
            {
                word = rootSentence;
            }
            string returnSentence = GrowRecursive(word, 0, highwayVariables);
            returnSentence = GrowRecursive(returnSentence, 0, sideStreetVariables);
            return returnSentence;
        }

        private string GrowRecursive(string word, int iterationIndex = 0, GenerationVariables genType = null)
        {
            if(genType == null)
            {
                throw new System.Exception("Generation Variable not passed to LSystemGenerator.GrowRecursive");
            }

            if (iterationIndex >= genType.iterationLimit)
            {
                return word;
            }
            StringBuilder newWord = new StringBuilder();

            foreach (var c in word)
            {
                newWord.Append(c);
                ProcessRulesRecursively(newWord, c, iterationIndex, genType);
            }

            return newWord.ToString();
        }

        private void ProcessRulesRecursively(StringBuilder newWord, char c, int iterationIndex, GenerationVariables genType)
        {
            foreach (var rule in genType.rules)
            {
                if(rule.letter == c.ToString())
                {
                    //from vid**
                    if (genType.randomIgnoreRuleModifier && iterationIndex > 1)
                    {
                        if(Random.value < genType.chanceToIgnoreRule)
                        {
                            return;
                        }
                    }
                    // end vid **
                    newWord.Append(GrowRecursive(rule.GetResult(ref highwayTurnLimit), iterationIndex +1, genType));
                }
            }
        }



        /*
        private string GrowRecursive(string word, int iterationIndex = 0, GenerationVariables genType = null)
        {
            if (iterationIndex >= iterationLimit)
            {
                return word;
            }
            StringBuilder newWord = new StringBuilder();

            foreach (var c in word)
            {
                newWord.Append(c);
                ProcessRulesRecursively(newWord, c, iterationIndex);
            }

            return newWord.ToString();
        }

        private void ProcessRulesRecursively(StringBuilder newWord, char c, int iterationIndex)
        {
            foreach (var rule in rules)
            {
                if(rule.letter == c.ToString())
                {
                    //from vid**
                    if (randomIgnoreRuleModifier && iterationIndex > 1)
                    {
                        if(Random.value < chanceToIgnoreRule)
                        {
                            return;
                        }
                    }
                    // end vid **
                    newWord.Append(GrowRecursive(rule.GetResult(), iterationIndex +1));
                }
            }
        }
        */



    }
}