using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SVS.SimpleVisualizer;

namespace SVS
{
    public class Visualizer : MonoBehaviour
    {
        public LSystemGenerator lsystem;
        List<Vector3> positions = new List<Vector3>();

        //from video https://www.youtube.com/watch?v=-sr_RFdMaz4&list=PLcRSafycjWFcbaI8Dzab9sTy5cAQzLHoy&index=8
        public RoadHelper roadHelper;
        public StructureHelper structureHelper;

        private int highwayLength = 16; //new
        private int length = 4;
        private float angle = 90;

        public int Length
        {
            get
            {
                if (length > 0)
                {
                    return length;
                }
                else
                {
                    return 1;
                }
            }
            set => length = value;
        }

        private void Start()
        {
            var sequence = lsystem.GenerateSentence();
            Debug.Log(sequence + "Visualizer");
            VisualizeSequence(sequence);
        }

        private void VisualizeSequence(string sequence)
        {
            Stack<AgentParameters> savePoints = new Stack<AgentParameters>();
            var currentPosition = Vector3.zero;

            Vector3 direction = Vector3.forward;
            Vector3 tempPosition = Vector3.zero;

            positions.Add(currentPosition);

            foreach (var letter in sequence)
            {
                EncodingLetters encoding = (EncodingLetters)letter;
                switch (encoding)
                {
                    case EncodingLetters.unknown:
                        break;
                    case EncodingLetters.save:
                        savePoints.Push(new AgentParameters
                        {
                            position = currentPosition,
                            direction = direction,
                            length = Length
                        });
                        break;
                    case EncodingLetters.load:
                        if (savePoints.Count > 0)
                        {
                            var agentParameter = savePoints.Pop();
                            currentPosition = agentParameter.position;
                            direction = agentParameter.direction;
                            Length = agentParameter.length;
                        }
                        else
                        {
                            throw new System.Exception("Don't have a saved point on the stack");
                        }
                        break;

                    case EncodingLetters.streetStart:
                        //reset the length value(don't know yet if this solves it
                        Length = 4;
                        positions.Add(currentPosition);
                        break; //NOTDONE?

                    case EncodingLetters.halfDraw:
                        tempPosition = currentPosition;
                        currentPosition += direction * (length / 2);
                        roadHelper.PlaceStreetPositions(tempPosition, Vector3Int.RoundToInt(direction), (length/2));
                        break;

                    case EncodingLetters.draw:
                        tempPosition = currentPosition;
                        currentPosition += direction * length;
                        roadHelper.PlaceStreetPositions(tempPosition, Vector3Int.RoundToInt(direction), length);
                        //if the road is an offshoot, reduce its length.
                        /*
                        if (direction != Vector3.forward || direction != (-1 * Vector3.forward))
                        {
                            Length -= 2;
                        }
                        */
                        positions.Add(currentPosition);
                        break;

                    case EncodingLetters.forward: //make it so no buildings spawn along here
                        tempPosition = currentPosition;
                        currentPosition += direction * highwayLength;
                        roadHelper.PlaceStreetPositions(tempPosition, Vector3Int.RoundToInt(direction), highwayLength, true);
                        break;

                    //letter 'I' is what highway rules iterates on, it does nothing else.
                    case EncodingLetters.highwayIterate:
                        //positions.Add(currentPosition);
                        break;

                    case EncodingLetters.turnRight:
                        direction = Quaternion.AngleAxis(angle, Vector3.up) * direction;
                        break;
                    case EncodingLetters.turnLeft:
                        direction = Quaternion.AngleAxis(-angle, Vector3.up) * direction;
                        break;
                    default:
                        break;
                }
            }

            roadHelper.FixRoad();
            structureHelper.PlaceStructuresAroundRoad(roadHelper.GetValidRoadPositions(), roadHelper.GetRoadPositions());
        }
    }
}

