using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SVS
{
    [Serializable]
    public class BuildingType
    {
        [SerializeField]
        private GameObject[] prefabs;
        //size needed for the house to be placed
        public int sizeRequired;
        //number of buildings that will appear in the scene
        public int quantity;
        //amount of buildings already placed on the map
        public int quantityAlreadyPlaced;

        public GameObject GetPrefab()
        {
            quantityAlreadyPlaced++;
            if(prefabs.Length > 1)
            {
                var random = UnityEngine.Random.Range(0, prefabs.Length);
                return prefabs[random];
            }
            return prefabs[0];
        }


        public bool IsBuildingAvailable()
        {
            return quantityAlreadyPlaced < quantity;
        }

        public void Reset()
        {
            quantityAlreadyPlaced = 0;
        }
    }
}


