using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SVS
{
    [CreateAssetMenu(menuName = "ProceduralCity/Rule")]
    public class Rule : ScriptableObject
    {
        //this is the letter the rule operates on
        public string letter;
        //this is the operation on the given letter
        [SerializeField]
        private string[] results = null;
        //these are special rules that can only happen a limited amount of times
        [SerializeField]
        private string[] specialGeneration = null;
        [SerializeField]
        private bool randomResult = false;

        public string GetResult(ref int specialLimit)
        {
            string[] possibleResults;

            //if specialLimit isn't zero combine results and specialGeneration
            //if specialLimit is zero exclude specialGeneration
            if (specialLimit > 0)
            {
                possibleResults = new string[results.Length + specialGeneration.Length];
                results.CopyTo(possibleResults, 0);
                specialGeneration.CopyTo(possibleResults, results.Length);
            } 
            else
            {
                possibleResults = new string[results.Length];
                results.CopyTo(possibleResults, 0);
            }

            //if randomresults is enabled, randomly choose an option.
            if(randomResult)
            {
                int randomIndex = UnityEngine.Random.Range(0, possibleResults.Length);

                //if randomIndex is one of the specialGenerations
                if(randomIndex >= results.Length)
                {
                    //decrement specialLimit
                    specialLimit--;
                }

                return possibleResults[randomIndex];
            }
            return possibleResults[0];
        }
    }
}