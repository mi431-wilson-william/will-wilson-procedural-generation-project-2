using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace SVS
{

    //from video https://www.youtube.com/watch?v=c9OTDfKJrAM&list=PLcRSafycjWFcbaI8Dzab9sTy5cAQzLHoy&index=10
    public class StructureHelper : MonoBehaviour
    {
        public BuildingType[] buildingTypes;
        public Dictionary<Vector3Int, GameObject> structuresDictionary = new Dictionary<Vector3Int, GameObject>();

        public void PlaceStructuresAroundRoad(List<Vector3Int> roadPositions, List<Vector3Int> totalRoadPositions)
        {
            //shuffle the list of road positions
            //This is code I yoinked from
            //https://forum.unity.com/threads/clever-way-to-shuffle-a-list-t-in-one-line-of-c-code.241052/
            List<Vector3Int> shuffledRoadPositions = roadPositions.OrderBy(x => UnityEngine.Random.value).ToList();

            Dictionary<Vector3Int, Direction> freeEstateSpots = FindFreeSpacesAroundRoad(shuffledRoadPositions, totalRoadPositions);
            Dictionary<Vector3Int, Direction> freeDowntownSpots = FindFreeSpacesAroundRoad(shuffledRoadPositions, totalRoadPositions);
            List<Vector3Int> blockedPositions = new List<Vector3Int>();

            foreach(var freeSpot in freeEstateSpots)
            {
                //when a big structure is placed, the spaces it takes are reserved for it.
                if (blockedPositions.Contains(freeSpot.Key))
                {
                    continue;
                }
                //rotate the house to face the road
                var rotation = Quaternion.identity;
                switch (freeSpot.Value)
                {
                    case Direction.Up:
                        rotation = Quaternion.Euler(0, 90, 0);
                        break;
                    case Direction.Down:
                        rotation = Quaternion.Euler(0, -90, 0);
                        break;
                    case Direction.Right:
                        rotation = Quaternion.Euler(0, 180, 0);
                        break;
                    default:
                        break;
                }

                //loop through each building types
                for(int i = 0; i < buildingTypes.Length; i++)
                {
                    //this case happens for the very last building
                    if(buildingTypes[i].quantity == -1)
                    {
                        var building = SpawnPrefab(buildingTypes[i].GetPrefab(), freeSpot.Key, rotation);
                        structuresDictionary.Add(freeSpot.Key, building);
                        break;
                    }
                    //if this building is available to place again
                    if (buildingTypes[i].IsBuildingAvailable())
                    {
                        //if this building needs more than one space
                        if(buildingTypes[i].sizeRequired > 1)
                        {
                            //this will reserve cell space for larger buildings
                            var halfSize = Mathf.FloorToInt(buildingTypes[i].sizeRequired / 2.0f);
                            List<Vector3Int> tempPositionsBlocked = new List<Vector3Int>();

                            if (VerifyIfBuildingFits(halfSize, freeEstateSpots, freeSpot, blockedPositions, ref tempPositionsBlocked))
                            {
                                //add all positins from tempPositionsBlocked to blockedPositions
                                blockedPositions.AddRange(tempPositionsBlocked);

                                var building = SpawnPrefab(buildingTypes[i].GetPrefab(), freeSpot.Key, rotation);
                                structuresDictionary.Add(freeSpot.Key, building);

                                foreach(var pos in tempPositionsBlocked)
                                {
                                    structuresDictionary.Add(pos, building);
                                }

                            }
                        } 
                        else
                        {
                            var building = SpawnPrefab(buildingTypes[i].GetPrefab(), freeSpot.Key, rotation);
                            structuresDictionary.Add(freeSpot.Key, building);
                        }
                        break;
                    }
                }
            }
        }

        //return true if there is space to fit a given building, false otherwise. 
        private bool VerifyIfBuildingFits(int halfSize, 
            Dictionary<Vector3Int, Direction> freeEstateSpots, 
            KeyValuePair<Vector3Int, Direction> freeSpot, 
            List<Vector3Int> blockedPositions,
            ref List<Vector3Int> tempPositionsBlocked)
        {
            Vector3Int direction = Vector3Int.zero;
            //if the road is up or down from the position of the building
            if(freeSpot.Value == Direction.Down || freeSpot.Value == Direction.Up)
            {
                direction = Vector3Int.right;
            } 
            else
            {
                direction = new Vector3Int(0, 0, 1);
            }

            for(int i = 1; i <= halfSize; i++)
            {
                var pos1 = freeSpot.Key + direction * i;
                var pos2 = freeSpot.Key - direction * i;

                //check if the positions adjacent are free; if they aren't free, return false
                if(!freeEstateSpots.ContainsKey(pos1) || !freeEstateSpots.ContainsKey(pos2)
                    || blockedPositions.Contains(pos1) || blockedPositions.Contains(pos2))
                {
                    return false;
                }
                tempPositionsBlocked.Add(pos1);
                tempPositionsBlocked.Add(pos2);
            }
            return true;
        }

        private GameObject SpawnPrefab(GameObject prefab, Vector3Int position, Quaternion rotation)
        {
            var newStructure = Instantiate(prefab, position, rotation, transform);
            return newStructure;
        }

        private Dictionary<Vector3Int, Direction> FindFreeSpacesAroundRoad(List<Vector3Int> roadPositions, List<Vector3Int> allRoadPositions)
        {
            Dictionary<Vector3Int, Direction> freeSpaces = new Dictionary<Vector3Int, Direction>();
            foreach(var position in roadPositions)
            {
                var neighborDirections = PlacementHelper.FindNeighbor(position, allRoadPositions);
                foreach(Direction direction in Enum.GetValues(typeof(Direction)))
                {
                    if(neighborDirections.Contains(direction) == false)
                    {
                        var newPosition = position + PlacementHelper.GetOffsetFromDirection(direction);
                        if (freeSpaces.ContainsKey(newPosition))
                        {
                            continue;
                        }
                        freeSpaces.Add(newPosition, PlacementHelper.GetReverseDirection(direction));
                    }
                }
            }
            return freeSpaces;
        }
    }
}

