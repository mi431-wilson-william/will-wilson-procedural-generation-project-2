using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace SVS
{

    //made with video 
    //https://www.youtube.com/watch?v=oMRTBPAJ0e8&list=PLcRSafycjWFcbaI8Dzab9sTy5cAQzLHoy&index=6
    public class SimpleVisualizer : MonoBehaviour
    {
        public LSystemGenerator lsystem;
        List<Vector3> positions = new List<Vector3>();

        public GameObject prefab;
        public Material lineMaterial;

        private int highwayLength = 16; //new
        private int length = 4;
        private float angle = 90;

        public int Length
        {
            get
            {
                if(length > 0)
                {
                    return length;
                } else
                {
                    return 1;
                }
            }
            set => length = value;  
        }

        private void Start()
        {
            var sequence = lsystem.GenerateSentence();
            Debug.Log(sequence + "SimpleVisualizer");
            VisualizeSequence(sequence);
        }

        private void VisualizeSequence(string sequence)
        {
            Stack<AgentParameters> savePoints = new Stack<AgentParameters>();
            var currentPosition = Vector3.zero;

            Vector3 direction = Vector3.forward;
            Vector3 tempPosition = Vector3.zero;

            positions.Add(currentPosition);

            foreach(var letter in sequence)
            {
                EncodingLetters encoding = (EncodingLetters)letter;
                switch (encoding)
                {
                    case EncodingLetters.unknown:
                        break;

                    case EncodingLetters.save:
                        savePoints.Push(new AgentParameters
                        {
                            position = currentPosition,
                            direction = direction,
                            length = Length
                        });
                        break;

                    case EncodingLetters.load:
                        if(savePoints.Count > 0)
                        {
                            var agentParameter = savePoints.Pop();
                            currentPosition = agentParameter.position;
                            direction = agentParameter.direction;
                            Length = agentParameter.length;
                        } else
                        {
                            throw new System.Exception("Don't have a saved point on the stack");
                        }
                        break;

                    case EncodingLetters.streetStart:
                        //Length -= 2;
                        //reset the length value(don't know yet if this solves it
                        Length = 4;
                        positions.Add(currentPosition);
                        break;

                    case EncodingLetters.halfDraw:
                        tempPosition = currentPosition;
                        currentPosition += direction * (length/2);
                        DrawLine(tempPosition, currentPosition, Color.red);
                        //make it a random chance to decrease the length
                        /*
                        if(Random.value >= 0.75)
                        {
                            Length -= 2;
                        }
                        */
                        //Length -= 2;
                        //positions.Add(currentPosition);
                        break;

                    case EncodingLetters.draw:
                        tempPosition = currentPosition;
                        currentPosition += direction * length;
                        DrawLine(tempPosition, currentPosition, Color.red);
                        //make it a random chance to decrease the length
                        /*
                        if(Random.value >= 0.75)
                        {
                            Length -= 2;
                        }
                        */
                        //Length -= 2;
                        //positions.Add(currentPosition);
                        break;

                    //new letter, H is for moving forward without having a rule operating on it; meaning, no branching paths.
                    case EncodingLetters.forward:
                        tempPosition = currentPosition;
                        currentPosition += direction * highwayLength;
                        DrawLine(tempPosition, currentPosition, Color.red);
                        break;

                    //letter 'I' is what highway rules iterates on, it does nothing else.
                    case EncodingLetters.highwayIterate:
                        //positions.Add(currentPosition);
                        break;

                    case EncodingLetters.turnRight:
                        direction = Quaternion.AngleAxis(angle, Vector3.up) * direction;
                        break;

                    case EncodingLetters.turnLeft:
                        direction = Quaternion.AngleAxis(-angle, Vector3.up) * direction;
                        break;

                    default:
                        break;
                }
            }

            foreach (var position in positions)
            {
                Instantiate(prefab, position, Quaternion.identity);
            }

        }

        private void DrawLine(Vector3 start, Vector3 end, Color color)
        {
            GameObject line = new GameObject("line");
            line.transform.position = start;
            var lineRenderer = line.AddComponent<LineRenderer>();
            lineRenderer.material = lineMaterial;
            lineRenderer.startColor = color;
            lineRenderer.endColor = color;
            lineRenderer.startWidth = 0.1f;
            lineRenderer.endWidth = 0.1f;
            lineRenderer.SetPosition(0, start);
            lineRenderer.SetPosition(1, end);
        }

        public enum EncodingLetters
        {
            unknown = '1',
            save = '[',
            load = ']',
            streetStart = 'S',//new
            draw = 'F',
            halfDraw = 'f',//new
            forward = 'H', //new
            highwayIterate = 'I', //new
            turnRight = '+',
            turnLeft = '-'
        }
    }

}

